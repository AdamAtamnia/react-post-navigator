
/**
 * React component that represents a the right hand side (An individual post)
 * @author Adam Atamnia
 */
class Rhs extends React.Component {
	/**
	* @param {post:Post} props 
	*/
	constructor(props){
		super(props)
	}

	render(){
		if(this.props.post){
			return <ul className="individualpost">
				<li>
					<span>Author: {this.props.post.author}</span>
					<span>Date: {this.props.post.date}</span>
				</li>
				<li>
					<p>Text: {this.props.post.text}</p>
				</li>
				<li>
					<span>Rate: {this.props.post.rate}</span>
					<span>Replies: {this.props.post.replies}</span>
					<span>Like: {this.props.post.like}</span>
				</li>   
			</ul> 
		} else {
			return <div></div>
		}
	}
}

