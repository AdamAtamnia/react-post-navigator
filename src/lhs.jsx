
/**
 * @author Noah Labrecque
 * ReactComponent represeting the left hand side, 
 * showing post categories to chose from
 */
class Lhs extends React.Component {
	/**
	 * @param {forumData:ForumData, setCategory:function} props 
	 */
	constructor (props) {
		super(props);
	}
	render () {
		return (
			<section id="LHS"> {
				this.props.forumData.categories.map((category) => 
					<button 
						key= { category.id }
						onClick= { () => this.props.setCategory(category) }> 
							{ category.name } 
					</button>
				)
			} </section>
		) 
	}
}
