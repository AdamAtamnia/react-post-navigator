// Fetching form data before rendering
/**
* @author Noah Labrecque, Adam Atamnia
*/
fetchFormData().then(() => {
	ReactDOM.render(
			<App forumData={document.forumData}></App>, 
			document.querySelector("main")
		);
})
