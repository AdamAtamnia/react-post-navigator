
/**
 * @author Noah Labrecque, Adam Atamnia
 * 
 * Fetches forum data from a json file and sets it as a 
 * property on the document to be used globaly
 * 
 * @returns Promise 
 */
async function fetchFormData () {
	return fetch("../data/forum.json")
	.then(responce => {
		if(!responce.ok){
			throw new Error(`${responce.status} ${responce.statusText}`)
		} else {
			return responce.json()
		}
	})
	.then(data => {
		document.forumData = data
		console.log(document.forumData)
	})
	.catch(err => console.log(err.message));
}