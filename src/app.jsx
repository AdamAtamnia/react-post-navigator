/**
 * Main App class
 */
class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			forumData: props.forumData,
			selectedCategory: undefined,
			selectedTopic: undefined,
			selectedPost: undefined
		}
	}

	/**
	 * @author Noah Labrecque
	 * Sets the post in the app's state
	 * 
	 * @param {post} post 
	 */
	setPost = (post) => {
		this.setState({selectedPost: post});
	}

	/**
	 * @author Noah Labrecque
	 * Sets the topic in the app's state
	 * 
	 * @param {topic} topic 
	 */
	setTopic = (topic) => {
		this.setState({selectedTopic: topic.id});
	}

	/**
	 * @author Noah Labrecque
	 * Sets the category in the app's state
	 * 
	 * @param {category} category 
	 */
	setCategory = (category) => {
		this.setState({selectedCategory: category.id});
	}

	/**
	 * @author Noah Labrecque
	 */
	render() {
		return (
			<div id="main">
				<Lhs 
					setCategory={this.setCategory} 
					forumData={document.forumData}
				></Lhs>

				<Middle
					setPost={this.setPost}
					selectedCategory={this.state.selectedCategory} 
					forumData={document.forumData}
				></Middle>

				<Rhs 
					post={this.state.selectedPost}
				></Rhs>
			</div>
			
		)
	}
}