/**
 * @author Noah Labrecque, Adam Atamnia
 * React component that represents a the middle (section with All the topics and buttons to choose the post)
 */
class Middle extends React.Component {

	/**
	* @param {forumData:ForumData, selectedCategory:number, setPost:function} props 
	*/
	constructor(props){
		super(props)
	}


	render(){
		// Using the id - 1 since the id's are 1 indexed and arrays are 0 indexed
		let selectedCategory = this.props.forumData.categories[this.props.selectedCategory-1]
		if(selectedCategory) {
			return <section id="middle"> {
				selectedCategory.topicList.map((topic) =>
				<div class="topic"  key={topic.id}>
					<h3> {topic.topic_title} </h3>
					<div>
						{topic.listPosts.map((post) =>
							<div class="post" key={post.id}>
								{post.text}
								<button onClick={() => { this.props.setPost(post) }}>show</button>  
							</div>
						)}
					</div>
				</div> 
				)}
			</section>
			
		} else {
			return <section id="middle"></section>
		}
	}
}

